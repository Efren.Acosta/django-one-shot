from django.urls import path
from todos.views import todo_list, todo_list_detail

urlpatterns = [
    path("todos/", todo_list, name="todo_list"),
    path("todos/<int:id>/", todo_list_detail, name="todo_list_detail"),
]
