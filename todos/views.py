from django.shortcuts import render, get_object_or_404
from todos.models import TodoList


def todo_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todolist_object": todolist,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_item": todo,
    }
    return render(request, "todos/detail.html", context)
